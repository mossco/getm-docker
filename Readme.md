<!--
SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
SPDX-FileCopyrightText: 2023-2024 Helmholtz-Zentrum hereon GmbH
SPDX-License-Identifier: CC0-1.0
-->

This is a collection of dockerfiles for a development and a 
runtime container for the General Estuarine Transport Model (GETM) 

With a local Docker daemon running, you can build the containers by issuing

```
docker build -f Dockerfile.alpine.gfortran-openmpi.getm-dev -t platipodium/getm:runtime .
```

# Executing the container

Mount the local work GETM setups to the image's work directory `~/setups`:

```
docker run -it -v ~/setups:/home/setups -t platipodium/getm:runtime /bin/bash
```

You can also provide the GETM executable as the last argument to the `docker
run` command, or execute it interactively in the container.

```
docker run -v ~/setups:/home/setups -t platipodium/getm:run /opt/bin/getm_parallel_curvilinear
```

You may have to update the configuration file with the `editscenario` utility

```
pip install editscenario
editscenario $(setup).xml -q -e nml . --schemadir=$(GETMDIR)/schemas --targetversion=getm-2.5
```

# Specific setups

## Elbe setup

```
ln -sf ./Setup/par_setup.28p.dat par_setup.dat
mpirun --allow-run-as-root --oversubscribe -np 28 /opt/bin/getm_parallel_curvilinear
```

## MOSSCO Deep Lake
```
mpirun --allow-run-as-root --oversubscribe -np 12 /opt/bin/getm_parallel_curvilinear
```
