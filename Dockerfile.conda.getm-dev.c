# Dockerfile for development on conda
#
# SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
# SPDX-License-Identifier: CC0-1.0
#
# Download base image alpine
FROM condaforge/mambaforge:latest as base

LABEL maintainer="carsten.lemmen@hereon.de"
LABEL version="0.1a"
LABEL description="This is a custom Docker image for a development container"

# Install a development toolchain based on gnu/openmpi, git and  build tools

RUN mamba install -y git
RUN mamba install -y cmake
RUN mamba install -y netcdf-fortran openmpi
RUN pip install editscenario
RUN pip install gotmgui
CMD ["/bin/bash"]

FROM base AS gotm

ENV GOTMDIR=/home/gotm
RUN git clone --depth=1 --recurse-submodules https://github.com/gotm-model/code.git $GOTMDIR
RUN mkdir -p $GOTMDIR/build
RUN mkdir -p /opt
RUN cmake -B $GOTMDIR/build -S $GOTMDIR -DCMAKE_INSTALL_PREFIX=/opt -DGOTM_USE_FABM=OFF
RUN make -C $GOTMDIR/build
RUN make -C $GOTMDIR/build install
CMD ["/bin/bash"]

FROM gotm AS getm
ENV GETMDIR=/home/getm
ENV GETM_BASE=$GETMDIR
ENV GOTM_BASE=$GOTMDIR

RUN git clone https://git.code.sf.net/p/getm/code -b iow --depth=1 $GETMDIR 
RUN mkdir $GETMDIR/build
RUN cmake -B $GETMDIR/build -S $GETMDIR/src -DCMAKE_INSTALL_PREFIX=/opt \
  -DGETM_USE_PARALLEL=ON \
  -DGETM_USE_STATIC=OFF \
  -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=ON \
	-DGETM_EMBED_VERSION=ON \
  -DCMAKE_Fortran_FLAGS="-O3" \
  -DGETM_FLAGS="-D_NEW_DAF_ -DCURVILINEAR" 
RUN make -C $GETMDIR/build
RUN make -C $GETMDIR/build install 
RUN mv /opt/bin/getm_cartesian_parallel /opt/bin/getm_parallel_curvilinear

RUN cmake -B $GETMDIR/build -S $GETMDIR/src -DCMAKE_INSTALL_PREFIX=/opt \
  -DGETM_USE_PARALLEL=OFF \
  -DGETM_USE_STATIC=OFF \
  -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=ON \
	-DGETM_EMBED_VERSION=ON \
  -DCMAKE_Fortran_FLAGS="-O3" \
  -DGETM_FLAGS="-D_NEW_DAF_ -DCURVILINEAR" 
RUN make -C $GETMDIR/build
RUN make -C $GETMDIR/build install 
RUN mv /opt/bin/getm_cartesian /opt/bin/getm_serial_curvilinear

CMD ["/bin/bash"]

FROM condaforge/mambaforge:latest as runtime
COPY --from=getm /opt /opt
COPY --from=getm /home/getm/schemas /opt/share/schemas

LABEL maintainer="carsten.lemmen@hereon.de"
LABEL version="0.1a"
LABEL description="This is a custom Docker image for a deployment container"
LABEL license="CC0-1.0"
LABEL copyright="2024 Helmholtz-Zentrum hereon GmbH"

# Install a development toolchain based on gnu/openmpi, git and  build tools
#RUN apk add --no-cache \
RUN mamba install -y git cmake 
RUN mamba install -y openmpi
RUN mamba install -y netcdf-fortran

RUN pip install editscenario
RUN pip install gotmgui

ENV PATH=${PATH}:/opt/bin
ENV LD_LIBRARY_PATH=${PATH}:/opt/lib

RUN groupadd -g 1009 model
RUN useradd -d /home -s /bin/bash -m model -u 1009 -g 1009
USER model
ENV HOME /home

CMD ["/bin/bash"]